##!/usr/bin/python

import socket
import sys
import thread
import queue

receivedDataFromCCS = queue.Queue()
dataToSendToCCS = queue.Queue()

# Passed data from local Rover modules to CCS
def PassLocalDataToCCS():
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Bind the socket to the port
    server_address = ('localhost', 4567)
    print >> sys.stderr, 'starting up on %s port %s' % server_address
    sock.bind(server_address)
    receivedCount = 0

    while True:
        data, address = sock.recvfrom(3000)
        receivedCount += 1
        print >> sys.stderr, 'RC: %s' % receivedCount

        #print >> sys.stderr, 'received %s bytes from %s' % (len(data), address)
        #print >> sys.stderr, data

        if data:
            # replace with line to pass data to CCS through datalink
            dataToSendToCCS.put(data)

def HardwareSendToCCS():
    print('Send Data To CCS')
    sentCount = 0
    # Create a UDP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    server_address = ('localhost', 4213)

    while True:
        dataToSend = dataToSendToCCS.get()

        # Send data
        sent = sock.sendto(dataToSend, server_address)
        sentCount += 1
        print >> sys.stderr, 'SC: %s' % sentCount

# Pass data from datalink to Rover
def PassDataFromCCSToRover():
    # Create a UDP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    server_address = ('localhost', 4211)

    while True:
        item = receivedDataFromCCS.get()
        
        # Send data
        sent = sock.sendto(item, server_address)

def HardwareReceiveFromCCS():
    print('Receive Data From CCS')
    # queue up data from CCS
    # receivedDataFromCCS.put(data_received)

#  Main
try:
    print(sys.version)
    print('Start thread 1')
    thread.start_new_thread(PassLocalDataToCCS, ())
    print('Start thread 2')
    thread.start_new_thread(PassDataFromCCSToRover, ())
    print('Start thread 3')
    thread.start_new_thread(HardwareSendToCCS, ())
    print('Start thread 4')
    thread.start_new_thread(HardwareReceiveFromCCS, ())
except:
   print('Error: unable to start thread')

while 1:
   pass